# 5d-chess-art

[![Pipeline Status](https://gitlab.com/5d-chess/5d-chess-js/badges/master/pipeline.svg)](https://gitlab.com/%{project_path}/-/commits/master)

Repository storing code to generate art for use in Chess in 5D (logos, animations, etc.)

## Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
